var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.05
var dimension = 17
var position = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  if (Math.floor(tan(frameCount * 0.25)) % 7 === 0) {
    for (var i = 0; i < position.length; i++) {
      for (var j = 0; j < position[i].length; j++) {
        position[i][j] = Math.floor(Math.random() * dimension)
      }
    }
  }
  if (Math.floor(tan(frameCount * 0.3)) % 9 === 0) {
    for (var i = 0; i < position.length; i++) {
      fill(128 + 128 * sin((frameCount * 0.05 * i) + i))
      noStroke()
      push()
      translate(windowWidth * 0.5 + (position[i][0] - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), windowHeight * 0.5 + (position[i][1] - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      rect(0, 0, boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      pop()

      push()
      translate(windowWidth * 0.5 + ((dimension - 1 - position[i][0]) - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), windowHeight * 0.5 + (position[i][1] - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      rect(0, 0, boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      pop()

      push()
      translate(windowWidth * 0.5 + (position[i][0] - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), windowHeight * 0.5 + ((dimension - 1 - position[i][1]) - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      rect(0, 0, boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      pop()

      push()
      translate(windowWidth * 0.5 + ((dimension - 1 - position[i][0]) - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), windowHeight * 0.5 + ((dimension - 1 - position[i][1]) - Math.floor(dimension * 0.5)) * boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      rect(0, 0, boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)), boardSize * initSize * (0.85 + 0.15 * sin(frameCount * 0.2 + i * frameCount * 0.075)))
      pop()
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
